from datetime import datetime
import discord

from zkb_redisq import ZkbRedisQ


class DiscordClient(discord.Client):
    def __init__(self, **options):
        super().__init__(**options)
        self.zkb_instances = []

    async def on_ready(self):
        print('Logged on as', self.user)

    async def on_message(self, message):
        # don't respond to ourselves
        if message.author == self.user:
            return

        if message.channel.type.name == "private":
            pass  # Para lo de los pings
        elif self.is_director(message.author):
            await self.director_commands(message)
        elif self.is_cuervo(message.author):
            await self.cuervo_commands(message)

    def is_cuervo(self, author):
        for role in author.roles:
            if str(role) == "Cuervo":
                return True
        return False
    
    def is_director(self, author):
        for role in author.roles:
            if str(role) == "Director":
                return True
        return False

    async def send_message(self, channel, message_str):
        await channel.send(message_str)

    async def cuervo_commands(self, message):
        if message.content == "!ping":
            await self._command_bang_ping(message.channel)
        elif message.content == "!evetime":
            await self._command_bang_evetime(message.channel)
        elif message.content.lower() == "larga vida":
            await self._command_larga_vida(message.channel)
        elif message.content.lower() == "gracias":
            await self._command_gracias(message.channel, message.author)

    async def director_commands(self, message):
        await self.cuervo_commands(message)
        if message.content == "!zkbstart":
            await self._command_director_bang_zkbstart(message.channel)
        if message.content == "!zkbstop":
            await self._command_director_bang_zkbstop(message.channel)

    async def _command_bang_ping(self, channel):
        await self.send_message(channel, "Pong")

    async def _command_bang_evetime(self, channel):
        naive_utc_dt = datetime.utcnow()
        await self.send_message(channel, naive_utc_dt.strftime("EVE Time Now: %d-%b-%Y (%H:%M:%S)"))

    async def _command_larga_vida(self, channel):
        await self.send_message(channel, "¡Larga vida al Martillo de Aníbal!")

    async def _command_gracias(self, channel, author):
        await self.send_message(channel, "Denada {}".format(author.mention))

    async def _command_director_bang_zkbstart(self, channel):
        zkb_instance = ZkbRedisQ(98500968, self, channel)
        zkb_instance.start()
        self.zkb_instances.append(zkb_instance)
        await self.send_message(channel, "Zkillboard posting iniciado")

    async def _command_director_bang_zkbstop(self, channel):
        for zkb_isntance in self.zkb_instances:
            if str(zkb_isntance.channel) == str(channel):
                zkb_isntance.stop()
                await self.send_message(channel, "Zkillboard posting detenido")
                break
