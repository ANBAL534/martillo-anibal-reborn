import asyncio
import threading
from time import sleep
import requests


class ZkbRedisQ(object):
    def __init__(self, corp_id, discord_client, channel):
        self.corp_id = corp_id
        self.discord_client = discord_client
        self.channel = channel
        self.stop_event = threading.Event()
        self.t = None

    def _run_thread(self):
        while not self.stop_event.is_set():
            response = requests.get("https://redisq.zkillboard.com/listen.php")
            if len(response.text) < 30:
                sleep(60)  # Wait for kills to buffer up
                continue  # Not a kill

            zkill_dict = response.json()["package"]["killmail"]

            # Check if the victim if from the selected corp
            if zkill_dict["victim"]["corporation_id"] == self.corp_id:
                zkb_link = "https://zkillboard.com/kill/{}/".format(str(zkill_dict["killmail_id"]))
                char_name = requests.get("https://esi.evetech.net/latest/characters/{}/?datasource=tranquility"
                                         .format(zkill_dict["victim"]["character_id"])).json()["name"]
                asyncio.get_event_loop().create_task(
                    self.discord_client.send_message(self.channel, "¡Maldito manco! {}. ¡Deja de palmar naves!\n{}"
                                                     .format(char_name, zkb_link)))
            else:  # Check if any of the attackers is from the corporation
                for attacker in zkill_dict["attackers"]:
                    if attacker["corporation_id"] == self.corp_id:
                        zkb_link = "https://zkillboard.com/kill/{}/".format(str(zkill_dict["killmail_id"]))
                        char_name = \
                            requests.get("https://esi.evetech.net/latest/characters/{}/?datasource=tranquility"
                                         .format(attacker["character_id"])).json()["name"]
                        asyncio.get_event_loop().create_task(
                            self.discord_client.send_message(self.channel, "¡Buenisima! {}. ¡Eres un crack!\n{}"
                                                             .format(char_name, zkb_link)))
            sleep(500)  # Do not spam zkb

    def start(self):
        self.stop_event.clear()
        self.t = threading.Thread(target=self._run_thread, args=())
        self.t.start()

    def stop(self):
        self.stop_event.set()
        self.t.join()
